#!/bin/bash

# dqib - Debian quick image baker
# Copyright © 2019 Giovanni Mascellani <gio@debian.org>

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

set -e

SYSTEM="$1"

if [ -z "$MEM" ] ; then
    if [ "$SYSTEM" == "m68k-q800" ] ; then
        MEM="1000M"
    else
        MEM="1G"
    fi
fi

if [ -z "$DISK_SIZE" ] ; then
    DISK_SIZE="10G"
fi

if [ -z "$SUITE" ] ; then
    SUITE="unstable"
fi

if [ -z "$MIRROR" ] ; then
    MIRROR="_default"
fi

if [ -z "$MIRROR2" ] ; then
    MIRROR2="_default"
fi

if [ -z "$DIR" ] ; then
    DIR=`mktemp -d /tmp/$SYSTEM-XXXXXXX`
fi
TARDISK="$DIR/image.tar.gz"
DISK="$DIR/image.qcow2"

FOUND="no"
PORTS="no"

if [ "$SYSTEM" == "i386-pc" ] ; then
    ARCH="i386"
    LINUX="linux-image-686"
    QEMU_ARCH="i386"
    QEMU_MACHINE="pc"
    QEMU_DISK="-drive file=image.qcow2"
    QEMU_CPU="coreduo"
    QEMU_NET_DEVICE="-device e1000,netdev=net"
    LINUX_FILENAME="vmlinuz"
    INITRD_FILENAME="initrd.img"
    CONSOLE="ttyS0"
    FOUND="yes"
fi

if [ "$SYSTEM" == "amd64-pc" ] ; then
    ARCH="amd64"
    LINUX="linux-image-amd64"
    QEMU_ARCH="x86_64"
    QEMU_MACHINE="pc"
    QEMU_CPU="Nehalem"
    QEMU_DISK="-drive file=image.qcow2"
    QEMU_NET_DEVICE="-device e1000,netdev=net"
    LINUX_FILENAME="vmlinuz"
    INITRD_FILENAME="initrd.img"
    CONSOLE="ttyS0"
    FOUND="yes"
fi

if [ "$SYSTEM" == "mipsel-malta" ] ; then
    ARCH="mipsel"
    LINUX="linux-image-4kc-malta"
    QEMU_ARCH="mipsel"
    QEMU_MACHINE="malta"
    QEMU_DISK="-drive file=image.qcow2"
    QEMU_CPU="4KEc"
    QEMU_NET_DEVICE="-device e1000,netdev=net"
    LINUX_FILENAME="vmlinux"
    INITRD_FILENAME="initrd.img"
    CONSOLE="ttyS0"
    FOUND="yes"
fi

if [ "$SYSTEM" == "mips64el-malta" ] ; then
    ARCH="mips64el"
    LINUX="linux-image-5kc-malta"
    QEMU_ARCH="mips64el"
    QEMU_MACHINE="malta"
    QEMU_CPU="5KEc"
    QEMU_DISK="-drive file=image.qcow2"
    QEMU_NET_DEVICE="-device e1000,netdev=net"
    LINUX_FILENAME="vmlinux"
    INITRD_FILENAME="initrd.img"
    CONSOLE="ttyS0"
    FOUND="yes"
fi

if [ "$SYSTEM" == "armel-raspi2" ] ; then
    ARCH="armel"
    LINUX="linux-image-rpi"
    QEMU_ARCH="arm"
    QEMU_MACHINE="raspi2"
    QEMU_CPU="cortex-a7"
    QEMU_DISK="-device virtio-blk-device,drive=hd -drive file=image.qcow2,if=none,id=hd"
    QEMU_NET_DEVICE="-device virtio-net-device,netdev=net"
    LINUX_FILENAME="vmlinuz"
    INITRD_FILENAME="initrd.img"
    CONSOLE="ttyAMA0"
    FOUND="yes"
fi

if [ "$SYSTEM" == "armhf-virt" ] ; then
    ARCH="armhf"
    LINUX="linux-image-armmp"
    QEMU_ARCH="arm"
    QEMU_MACHINE="virt"
    QEMU_CPU="cortex-a15"
    QEMU_DISK="-device virtio-blk-device,drive=hd -drive file=image.qcow2,if=none,id=hd"
    QEMU_NET_DEVICE="-device virtio-net-device,netdev=net"
    LINUX_FILENAME="vmlinuz"
    INITRD_FILENAME="initrd.img"
    CONSOLE="ttyAMA0"
    FOUND="yes"
fi

if [ "$SYSTEM" == "arm64-virt" ] ; then
    ARCH="arm64"
    LINUX="linux-image-arm64"
    QEMU_ARCH="aarch64"
    QEMU_MACHINE="virt"
    QEMU_CPU="cortex-a57"
    QEMU_DISK="-device virtio-blk-device,drive=hd -drive file=image.qcow2,if=none,id=hd"
    QEMU_NET_DEVICE="-device virtio-net-device,netdev=net"
    LINUX_FILENAME="vmlinuz"
    INITRD_FILENAME="initrd.img"
    CONSOLE="ttyAMA0"
    FOUND="yes"
fi

if [ "$SYSTEM" == "s390x-virt" ] ; then
    ARCH="s390x"
    LINUX="linux-image-s390x"
    QEMU_ARCH="s390x"
    QEMU_MACHINE="s390-ccw-virtio"
    QEMU_CPU="z900"
    QEMU_DISK="-drive file=image.qcow2"
    QEMU_NET_DEVICE="-device virtio-net-ccw,netdev=net"
    LINUX_FILENAME="vmlinuz"
    INITRD_FILENAME="initrd.img"
    CONSOLE="ttyAMA0"
    FOUND="yes"
fi

if [ "$SYSTEM" == "ppc64el-pseries" ] ; then
    ARCH="ppc64el"
    LINUX="linux-image-powerpc64le"
    QEMU_ARCH="ppc64le"
    QEMU_MACHINE="pseries"
    QEMU_CPU="power9"
    QEMU_DISK="-drive file=image.qcow2"
    QEMU_NET_DEVICE="-device e1000,netdev=net"
    LINUX_FILENAME="vmlinux"
    INITRD_FILENAME="initrd.img"
    CONSOLE="hvc0"
    FOUND="yes"
fi

if [ "$SYSTEM" == "m68k-q800" ] ; then
    ARCH="m68k"
    LINUX="linux-image-m68k"
    QEMU_ARCH="m68k"
    QEMU_MACHINE="q800"
    QEMU_CPU="m68040"
    QEMU_DISK="-drive file=image.qcow2"
    QEMU_NET_DEVICE="-net nic,model=dp83932,netdev=net"
    LINUX_FILENAME="vmlinuz"
    INITRD_FILENAME="initrd.img"
    CONSOLE="ttyS0"
    PORTS="yes"
    FOUND="yes"
fi

if [ "$SYSTEM" == "riscv64-virt" ] ; then
    ARCH="riscv64"
    LINUX="linux-image-riscv64"
    QEMU_ARCH="riscv64"
    QEMU_MACHINE="virt"
    QEMU_CPU="rv64"
    QEMU_DISK="-device virtio-blk-device,drive=hd -drive file=image.qcow2,if=none,id=hd"
    QEMU_NET_DEVICE="-device virtio-net-device,netdev=net"
    LINUX_FILENAME="vmlinuz"
    INITRD_FILENAME="initrd.img"
    CONSOLE="ttyS0"
    PORTS="yes"
    FOUND="yes"
fi

if [ "$SYSTEM" == "sh4-r2d" ] ; then
    ARCH="sh4"
    LINUX="linux-image-sh7751r"
    QEMU_ARCH="sh4"
    QEMU_MACHINE="r2d"
    QEMU_CPU="sh7751r"
    QEMU_DISK="-drive file=image.qcow2"
    QEMU_NET_DEVICE="-device e1000,netdev=net"
    LINUX_FILENAME="vmlinuz"
    INITRD_FILENAME="initrd.img"
    CONSOLE="tty0"
    PORTS="yes"
    FOUND="yes"
fi

if [ "$SYSTEM" == "powerpc-g3beige" ] ; then
    ARCH="powerpc"
    LINUX="linux-image-powerpc-smp"
    QEMU_ARCH="ppc"
    QEMU_MACHINE="g3beige"
    QEMU_CPU="g4"
    QEMU_DISK="-drive file=image.qcow2"
    QEMU_NET_DEVICE="-device e1000,netdev=net"
    LINUX_FILENAME="vmlinux"
    INITRD_FILENAME="initrd.img"
    CONSOLE="ttyPZ0"
    PORTS="yes"
    FOUND="yes"
fi

if [ "$SYSTEM" == "ppc64-pseries" ] ; then
    ARCH="ppc64"
    LINUX="linux-image-powerpc64"
    QEMU_ARCH="ppc64le"
    QEMU_MACHINE="pseries"
    QEMU_CPU="power9"
    QEMU_DISK="-drive file=image.qcow2"
    QEMU_NET_DEVICE="-device e1000,netdev=net"
    LINUX_FILENAME="vmlinux"
    INITRD_FILENAME="initrd.img"
    CONSOLE="hvc0"
    PORTS="yes"
    FOUND="yes"
fi

if [ "$SYSTEM" == "hppa-hppa" ] ; then
    ARCH="hppa"
    LINUX="linux-image-parisc"
    QEMU_ARCH="hppa"
    QEMU_MACHINE="hppa"
    QEMU_CPU="hppa"
    QEMU_DISK="-drive file=image.qcow2"
    QEMU_NET_DEVICE="-device e1000,netdev=net"
    LINUX_FILENAME="vmlinux"
    INITRD_FILENAME="initrd.img"
    CONSOLE="ttyS0"
    PORTS="yes"
    FOUND="yes"
fi

if [ "$SYSTEM" == "alpha-clipper" ] ; then
    ARCH="alpha"
    LINUX="linux-image-alpha-smp"
    QEMU_ARCH="alpha"
    QEMU_MACHINE="clipper"
    QEMU_CPU="ev68-alpha-cpu"
    QEMU_DISK="-drive file=image.qcow2"
    QEMU_NET_DEVICE="-device e1000,netdev=net"
    LINUX_FILENAME="vmlinuz"
    INITRD_FILENAME="initrd.img"
    CONSOLE="ttyS0"
    PORTS="yes"
    FOUND="yes"
fi

if [ "$SYSTEM" == "sparc64-sun4u" ] ; then
    ARCH="sparc64"
    LINUX="linux-image-sparc64-smp"
    QEMU_ARCH="sparc64"
    QEMU_MACHINE="sun4u"
    QEMU_CPU="sparc64"
    QEMU_DISK="-drive file=image.qcow2"
    QEMU_NET_DEVICE="-device e1000,netdev=net"
    LINUX_FILENAME="vmlinuz"
    INITRD_FILENAME="initrd.img"
    CONSOLE="hvc0"
    PORTS="yes"
    FOUND="yes"
fi

if [ "$FOUND" != "yes" ] ; then
    echo "Could not find system type: $SYSTEM"
    exit 1
fi

if [ "$MIRROR" == "_default" ] ; then
    if [ "$PORTS" == "no" ] ; then
        MIRROR="deb [arch=$ARCH] http://deb.debian.org/debian $SUITE main"
        KEYRING="/usr/share/keyrings/debian-archive-keyring.gpg"
    else
        MIRROR="deb [arch=$ARCH] http://deb.debian.org/debian-ports $SUITE main"
        KEYRING="/usr/share/keyrings/debian-ports-archive-keyring.gpg"
    fi
fi

if [ "$MIRROR2" == "_default" ] ; then
    if [ "$PORTS" == "no" ] ; then
        MIRROR2="_no"
    else
        MIRROR2="deb [arch=$ARCH] http://deb.debian.org/debian-ports unreleased main"
    fi
fi

QEMU_NET="$QEMU_NET_DEVICE -netdev user,id=net,hostfwd=tcp::2222-:22"

set -v

# Create the filesystem
if [ "$MIRROR2" == "_no" ] ; then
    mmdebstrap --architectures="$ARCH" --variant=required --include="$LINUX",debian-ports-archive-keyring --verbose "$SUITE" "$DIR"/chroot "$MIRROR"
else
    mmdebstrap --architectures="$ARCH" --variant=required --include="$LINUX",debian-ports-archive-keyring --verbose "$SUITE" "$DIR"/chroot "$MIRROR" "$MIRROR2"
fi

# Install a simple fstab and set hostname
cp fstab "$DIR"/chroot/etc/fstab
echo "debian" > "$DIR"/chroot/etc/hostname

# Create and set passwords for root and user debian
chroot "$DIR"/chroot adduser --gecos "Debian user,,," --disabled-password debian
echo "root:root" | chroot "$DIR"/chroot chpasswd
echo "debian:debian" | chroot "$DIR"/chroot chpasswd

# Update APT database, install important packages (except vim-* and
# isc-dhcp-*, which often fail to install) and openssh-server
chroot "$DIR"/chroot apt-get update
chroot "$DIR"/chroot bash -c 'cat /var/lib/apt/lists/*_Packages  | grep '\''^\(Package\|Priority\): '\'' | grep -B 1 '\''^Priority: important'\'' | grep ^Package | cut -d'\'' '\'' -f2 | grep -v ^vim | grep -v ^isc-dhcp | xargs apt-get install -y --no-install-recommends'
chroot "$DIR"/chroot apt-get install -y --no-install-recommends openssh-server

# Disable predictable interface naming and configure standard QEMU
# user network
ln -s /dev/null "$DIR"/chroot/etc/systemd/network/99-default.link
cp interfaces "$DIR"/chroot/etc/network/interfaces
cp resolv.conf "$DIR"/chroot/etc

# Configure SSH to permit password root login and have static SSH
# keys. This is not very secure, but these images are not meant for
# production anyway. And whatever key we put inside, it will always be
# public, so let's at least make them convenient.
cp keys/ssh_host_* "$DIR"/chroot/etc/ssh
chmod 600 "$DIR"/chroot/etc/ssh/ssh_host_*_key
echo 'PermitRootLogin yes' > "$DIR"/chroot/etc/ssh/sshd_config.d/permit_root.conf
mkdir -p "$DIR"/chroot/root/.ssh
mkdir -p "$DIR"/chroot/home/debian/.ssh
cat keys/ssh_user_*.pub > "$DIR"/chroot/root/.ssh/authorized_keys
cat keys/ssh_user_*.pub > "$DIR"/chroot/home/debian/.ssh/authorized_keys
chroot "$DIR"/chroot chown -Rc debian:debian /home/debian/.ssh
cp keys/ssh_user_*_key "$DIR"

# Recreate initrd
chroot "$DIR"/chroot update-initramfs -k all -c

# Riscv64 specific things
if [ "$SYSTEM" == "riscv64-virt" ] ; then
    chroot "$DIR"/chroot apt-get install -y u-boot-menu
    chroot "$DIR"/chroot ln -sf /dev/null /etc/systemd/system/serial-getty@hvc0.service
    cat >> "$DIR"/chroot/etc/default/u-boot <<EOF
U_BOOT_PARAMETERS="rw noquiet root=LABEL=rootfs"
U_BOOT_FDT_DIR="noexist"
EOF
    chroot "$DIR"/chroot u-boot-update
fi

# Create the tar archive
tar -cz -S -f "$TARDISK" -C "$DIR"/chroot .

# Extract kernel and initrd
ln -L "$DIR"/chroot/"$LINUX_FILENAME" "$DIR"/kernel
ln -L "$DIR"/chroot/"$INITRD_FILENAME" "$DIR"/initrd

# Convert the disk to a QEMU image
if [ "$DISK_SIZE" != "0" ] ; then
    qemu-img convert -f qcow2 "$DISK" -O qcow2 "$TARDISK"
    rm "$TARDISK"
fi

set +v

# Propose a boot command line
echo "This is a Debian image generated by the Debian Quick Image Baker." >> "$DIR"/readme.txt
echo -n "It was created on " >> "$DIR"/readme.txt
LANG=C LC_ALL=C TZ=UTC date >> "$DIR"/readme.txt
echo "See https://gitlab.com/giomasce/dqib for more information." >> "$DIR"/readme.txt
if [ "$SYSTEM" == "riscv64-virt" ] ; then
    echo "You need to install packages opensbi and u-boot-qemu" >> "$DIR"/readme.txt
    KERNEL_CMD="-bios /usr/lib/riscv64-linux-gnu/opensbi/generic/fw_jump.elf -kernel /usr/lib/u-boot/qemu-riscv64_smode/uboot.elf -object rng-random,filename=/dev/urandom,id=rng -device virtio-rng-device,rng=rng"
else
    KERNEL_CMD="-kernel kernel -initrd initrd"
fi
echo "(Try to) boot with:" >> "$DIR"/readme.txt
echo qemu-system-"$QEMU_ARCH" -machine "$QEMU_MACHINE" -cpu "$QEMU_CPU" -m "$MEM" $QEMU_DISK $QEMU_NET $KERNEL_CMD -nographic -append '"'root=LABEL=rootfs console="$CONSOLE"'"' >> "$DIR"/readme.txt
echo "You can use Ctrl-a x to exit from QEMU." >> "$DIR"/readme.txt
echo "You can login with root:root or debian:debian, or using the identities ssh_user_*_key in this directory." >> "$DIR"/readme.txt
echo "You can also login with SSH on the local port 2222 (this will give you a much better terminal emulation than the QEMU default)." >> "$DIR"/readme.txt
echo "Some machines support a video interface, which you can enable removing the -nographic option and possibly also the console= kernel argument." >> "$DIR"/readme.txt

echo "=== readme.txt content ==="
cat "$DIR"/readme.txt

rm -fr "$DIR"/chroot
